package service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import domain.Politico;
import domain.PoliticoDAO;

public class PoliticoService {

	private PoliticoDAO politicodao = new PoliticoDAO();

	public List<Politico> getTodos() {

		try {
			return politicodao.getTodos();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ArrayList<Politico>();
		}
	}

	public Politico getById(Long id) {
	try {
		return politicodao.getPoliticoById(id);
	}catch (Exception e) {
		System.out.println(e.toString());
	}
	return null;
	}

	public void salvarPolitico(Politico politico) {
		
		try {
			politicodao.salvar(politico);
		}catch (Exception e) {
			System.out.println(e.toString() +   "METODO - SALVAR");		
		}
		
	}
	
	public boolean delete(Long id) {
		try {
			return politicodao.delete(id);
		}catch (Exception e) {
			System.out.println(e.toString());
		}
		return false;
	}
}
