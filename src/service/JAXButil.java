package service;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.stream.XMLStreamWriter;

import org.codehaus.jettison.mapped.MappedNamespaceConvention;
import org.codehaus.jettison.mapped.MappedXMLStreamReader;
import org.codehaus.jettison.mapped.MappedXMLStreamWriter;

import domain.ListaPoliticos;
import domain.Politico;
import utils.Response;

public class JAXButil {
	
	private static JAXButil instance;
	private static JAXBContext context;
	
	public static JAXButil getInstance() {
		return instance;
	}
	
	static {
		try {
			context = JAXBContext.newInstance(Politico.class, ListaPoliticos.class, Response.class);
		}catch (JAXBException e) {
			System.out.println(e.toString());
		}
	}
	
	public static String toJSON(Object object) {
		try {
		StringWriter writer = new StringWriter();
		Marshaller m = context.createMarshaller();
		MappedNamespaceConvention con = new MappedNamespaceConvention();
		XMLStreamWriter xmlStreamWriter = new MappedXMLStreamWriter(con, writer);
		m.marshal(object, xmlStreamWriter);
		return writer.toString();
		
		}catch (JAXBException  e) {
			System.out.println(e.toString());
			return null;
		}
	}

}
