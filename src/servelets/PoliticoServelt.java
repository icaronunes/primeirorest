package servelets;

import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import domain.Politico;
import service.PoliticoService;


@WebServlet("/hello")
public class PoliticoServelt extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<Politico> lista = new PoliticoService().getTodos();
			//resp.getWriter().println("TESTE");
			//resp.getWriter().println(lista.toString());
	}
	
	
	@Override
	public void service(ServletRequest req, ServletResponse resp) throws ServletException, IOException {
		List<Politico> lista = new PoliticoService().getTodos();
		resp.getWriter().println(lista.toString());
	}


}
