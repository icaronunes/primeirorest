package servelets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import domain.ListaPoliticos;
import domain.Politico;
import domain.PoliticoDAO;
import service.JAXButil;
import service.PoliticoService;
import utils.RegexUtil;
import utils.Response;
import utils.ServletUtil;

@WebServlet("/politicos/*")
public class PoliticosServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private PoliticoService service = new PoliticoService();
	
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String requestUri = req.getRequestURI();
		Long id = RegexUtil.matchId(requestUri);

		if (id != null) {
			Politico politico = service.getById(id);
			System.out.println("doGet - id");
			if (politico != null) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				String json = gson.toJson(politico);
				ServletUtil.writeJSON(resp, json);
			} else {
				resp.sendError(404, "Politico n�o encontrado");
			}

		} else {
			System.out.println("doGet - all");
			List<Politico> politicos = service.getTodos();
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String json = gson.toJson(politicos);

			ServletUtil.writeJSON(resp, json);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Politico politico = new Politico();
		
		politico = getPoliticoFromParame(req);
		
		service.salvarPolitico(politico);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		String json = gson.toJson(politico);
		ServletUtil.writeJSON(resp, json);
		
	}

	private Politico getPoliticoFromParame(HttpServletRequest req) {
		Politico p = new Politico();
		String id = req.getParameter("id");
		
		if (id != null) {
			p = service.getById(Long.parseLong(id));
		}
		
		p.setNome(req.getParameter("nome"));
		p.setImagem(req.getParameter("image"));
		p.setPartido(req.getParameter("partido"));
		p.setResumo(req.getParameter("resumo"));
		p.setVisitas(Integer.parseInt(req.getParameter("visitas")));
		p.setAtivo(Integer.parseInt(req.getParameter("ativo")) == 1);

		return p;
	}
	
	@Override
	protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		Long id = RegexUtil.matchId(req.getRequestURI());
		if (id != null) {
			if (service.delete(id)) {
				Response r = Response.Ok("Politico Excluido com sucesso");
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				ServletUtil.writeJSON(resp, gson.toJson(r));
			} else {
				Response r = Response.Error("Politico ERRO ao excluido politico");
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				ServletUtil.writeJSON(resp, gson.toJson(r));
			}
			
		} else {
			Response r = Response.Ok("Url invalida - 404");
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			ServletUtil.writeJSON(resp, gson.toJson(r));
		
		}
	}

}
