package domain;

import java.io.Serializable;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="politicos")
public class ListaPoliticos implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<Politico> politicos;

	@XmlElement(name="politico")
	public List<Politico> getCarros() {
		return politicos;
	}

	public void setCarros(List<Politico> politicos) {
		this.politicos = politicos;
	}

	@Override
	public String toString() {
		return "ListaCarros [Politico=" + politicos + "]";
	}
}
