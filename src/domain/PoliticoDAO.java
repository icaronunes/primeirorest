package domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Statement;

public class PoliticoDAO extends BaseDAO {

	public Politico getPoliticoById(Long id) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = getConnection();
			stmt = conn.prepareStatement("select * from politicos where id=?");
			stmt.setLong(1, id);
			ResultSet resultSet = stmt.executeQuery();

			if (resultSet.next()) {
				Politico politico = createPolitico(resultSet);
				resultSet.close();
				return politico;
			}

		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}
		return null;

	}

	private Politico createPolitico(ResultSet rs) throws SQLException {
		Politico p = new Politico();
		p.setId(rs.getLong("id"));
		p.setNome(rs.getString("nome"));
		p.setAtivo(rs.getBoolean("ativo"));
		p.setImagem(rs.getString("image"));
		p.setPartido(rs.getString("partido"));
		p.setResumo(rs.getString("resumo"));
		p.setVisitas(rs.getInt("visitas"));

		return p;
	}

	public List<Politico> getTodos() throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		List<Politico> politicos = new ArrayList<>();
		try {
			conn = getConnection();
			stmt = conn.prepareStatement("select * from politicos");
			ResultSet resultSet = stmt.executeQuery();

			while (resultSet.next()) {
				Politico politico = createPolitico(resultSet);
				politicos.add(politico);
			}
			resultSet.close();
			return politicos;
		} catch (Exception e) {
			System.out.println(e.toString());
			return politicos;
		} finally {
			if (stmt != null) {
				stmt.close();
			}
			if (conn != null) {
				conn.close();
			}
		}

	}

	public void salvar(Politico politico) throws SQLException {
		Connection conn = null;
		PreparedStatement stmt = null;
		conn = getConnection();
		try {
			if (politico.getId() == null) {
				stmt = conn.prepareStatement("insert into politicos (nome, ativo, image, partido, resumo, visitas) "
						+ "VALUES (?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
			} else {
				stmt = conn.prepareStatement(
						"update politicos set nome=?, ativo=?, image=?, partido=?, resumo=?, visitas=?"
								+ " WHERE id=?");
			}

			stmt.setString(1, politico.getNome());
			stmt.setBoolean(2, politico.isAtivo());
			stmt.setString(3, politico.getImagem());
			stmt.setString(4, politico.getPartido());
			stmt.setString(5, politico.getResumo());
			stmt.setInt(6, politico.getVisitas());

			if (politico.getId() != null) {
				stmt.setLong(7, politico.getId());
			}

			int retorno = stmt.executeUpdate();

			if (retorno == 0) {
				throw new SQLException("Erro ao insetir politico");
			}

			if (politico.getId() == null) {
				Long id = getGeneratedId(stmt);
				politico.setId(id);
			}
		} finally {
			if (stmt != null) {
				stmt.close();
			}

			if (conn != null) {
				conn.close();
			}
		}

	}

	public boolean delete(Long id) throws SQLException {

		Connection conn = null;
		PreparedStatement stmt = null;

		try {
			conn = getConnection();
			stmt = conn.prepareStatement("DELETE FROM politicos WHERE id=?");
			stmt.setLong(1, id);
			int resultado = stmt.executeUpdate();
			return resultado > 0;

		} finally {
			if (conn != null) {
				conn.close();
			}

			if (stmt != null) {
				stmt.close();
			}
		}

	}

	private Long getGeneratedId(PreparedStatement stmt) throws SQLException {
		ResultSet rs = stmt.getGeneratedKeys();
		if (rs.next()) {
			Long id = rs.getLong(1);
			return id;
		}
		return 0L;
	}

}
