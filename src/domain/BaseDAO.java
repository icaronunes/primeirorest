package domain;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDAO {
	
	public BaseDAO() {
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected Connection getConnection() throws SQLException{
		
		String url = "jdbc:mysql://localhost/politicos";
		return DriverManager.getConnection(url, "icaro", "knabs123");
	}
	
	public static void main(String[] args) throws SQLException {
		
		BaseDAO db = new BaseDAO();
		Connection con = db.getConnection();
		System.out.println(con);
		
	}

}
