package utils;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Response {

	private	String status, msg;
	
	
	public static Response Ok (String msg) {
		Response r = new Response();
		r.setMsg(msg);
		r.setStatus("OK");
		
		return r;
	}
	
	public static Response Error (String msg) {
		Response r = new Response();
		r.setMsg(msg);
		r.setStatus("ERRO");
		
		return r;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	
	
}
